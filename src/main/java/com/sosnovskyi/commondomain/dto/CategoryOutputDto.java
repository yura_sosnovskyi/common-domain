package com.sosnovskyi.commondomain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CategoryOutputDto {

    private Long id;

    @NotBlank
    private String name;

    private String description;

    private String fullPath;

    private Long parentCategoryId;

    private List<CategoryOutputDto> subCategories;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
