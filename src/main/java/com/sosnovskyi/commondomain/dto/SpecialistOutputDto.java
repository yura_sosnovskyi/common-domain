package com.sosnovskyi.commondomain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.sosnovskyi.commondomain.model.EducationType;
import com.sosnovskyi.commondomain.model.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SpecialistOutputDto {

    private Long id;

    private String email;

    private String firstname;

    private String lastname;

    private String phoneNumber;

    private String city;

    private Integer age;

    private Gender gender;

    private List<String> roles;

    private String title;

    private EducationType educationType;

    private String educationInstitution;

    private String description;

    private BigDecimal rating;

    private Boolean isVerified;

    private List<FeedbackOutputDto> feedbacks;

    private List<String> categories;

    private Integer experience;

    private Set<SpecialistOutputDto> lastVisitedSpecialists;

    private String createdBy;

    private String updatedBy;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
