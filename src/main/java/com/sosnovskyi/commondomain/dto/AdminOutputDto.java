package com.sosnovskyi.commondomain.dto;

import com.sosnovskyi.commondomain.model.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
public class AdminOutputDto {

    private Long id;

    private String email;

    private String firstname;

    private String lastname;

    private String phoneNumber;

    private String telegramUsername;

    private String city;

    private Integer age;

    private Gender gender;

    private List<String> roles;

    private Set<SpecialistOutputDto> lastVisitedSpecialists;

    private String createdBy;

    private String updatedBy;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
