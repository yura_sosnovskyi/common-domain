package com.sosnovskyi.commondomain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CategoryInputDto {

    @NotBlank
    private String name;

    private String description;

    private Long parentCategoryId;

    // 1. subCategories field is ignored when updating
    // 2. parentCategoryId should be empty in subCategories to keep the hierarchy correct
    private List<CategoryInputDto> subCategories;
}
