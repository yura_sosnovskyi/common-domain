package com.sosnovskyi.commondomain.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class FeedbackInputDto {

    private String message;

    @Positive
    private Integer rating;

    @NotNull
    private Long specialistId;

    @NotNull
    private Long reviewerId;
}
