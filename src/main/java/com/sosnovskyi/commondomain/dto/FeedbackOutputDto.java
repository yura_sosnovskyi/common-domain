package com.sosnovskyi.commondomain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class FeedbackOutputDto {
    private Long id;
    private String message;
    private Integer rating;
    private Long specialistId;
    private Long reviewerId;
    private String reviewerName;
    private String createdBy;
    private String updatedBy;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
