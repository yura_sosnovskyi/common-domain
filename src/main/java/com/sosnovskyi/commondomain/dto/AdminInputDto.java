package com.sosnovskyi.commondomain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.sosnovskyi.commondomain.model.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
public class AdminInputDto {
    @NotBlank
    private String email;

    @NotBlank
    private String firstname;

    @NotBlank
    private String lastname;

    @NotBlank
    private String phoneNumber;

    private String telegramUsername;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;

    @NotBlank
    private String city;

    private Integer age;

    private Gender gender;
}
