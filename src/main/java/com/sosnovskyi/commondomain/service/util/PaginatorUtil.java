package com.sosnovskyi.commondomain.service.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Map;

public class PaginatorUtil {

    public static final String DEFAULT_PAGE_SIZE = "50";
    public static final String DEFAULT_PAGE_NUMBER = "0";

    public static Pageable createPageable(Map<String, String> params) {
        String sortBy = params.get("sortBy");
        String sortDirection = params.get("sortDirection");
        int pageSize = Integer.parseInt(params.getOrDefault("pageSize", DEFAULT_PAGE_SIZE));
        int pageNumber = Integer.parseInt(params.getOrDefault("pageNumber", DEFAULT_PAGE_NUMBER));

        Sort sort = sortBy == null || sortBy.isBlank()
                ? Sort.unsorted()
                : Sort.by(Sort.Direction.fromOptionalString(sortDirection).orElse(Sort.Direction.DESC), sortBy);

        return PageRequest.of(pageNumber, pageSize, sort);

    }
}
