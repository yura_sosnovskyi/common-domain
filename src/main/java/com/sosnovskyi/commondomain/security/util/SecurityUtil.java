package com.sosnovskyi.commondomain.security.util;

import com.sosnovskyi.commondomain.dto.UserOutputDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {

    public static UserOutputDto getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            throw new AuthenticationException("Cannot find authenticated user!") {
                @Override
                public String getMessage() {
                    return super.getMessage();
                }
            };
        }
        return (UserOutputDto) authentication.getPrincipal();
    }
}
