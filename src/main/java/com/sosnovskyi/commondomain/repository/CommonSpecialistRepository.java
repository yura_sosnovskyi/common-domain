package com.sosnovskyi.commondomain.repository;

import com.sosnovskyi.commondomain.model.Specialist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonSpecialistRepository extends JpaRepository<Specialist, Long> {

}
