package com.sosnovskyi.commondomain.repository;

import com.sosnovskyi.commondomain.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonCategoryRepository extends JpaRepository<Category, Long> {

}
