package com.sosnovskyi.commondomain.repository;

import com.sosnovskyi.commondomain.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonRoleRepository extends JpaRepository<Role, Long> {

}
