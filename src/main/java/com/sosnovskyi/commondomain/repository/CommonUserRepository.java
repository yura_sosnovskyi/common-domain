package com.sosnovskyi.commondomain.repository;

import com.sosnovskyi.commondomain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonUserRepository extends JpaRepository<User, Long> {

}
