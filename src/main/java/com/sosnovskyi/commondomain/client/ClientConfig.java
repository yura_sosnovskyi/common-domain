package com.sosnovskyi.commondomain.client;

import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class ClientConfig {

    @Value("${client.config.default.connectTimeout}")
    private Integer connectTimeout;

    @Value("${client.config.default.readTimeout}")
    private Integer readTimeout;

    @Value("${client.config.default.writeTimeout}")
    private Integer writeTimeout;

    @Bean
    public OkHttpClient requestInterceptor() {
        return new OkHttpClient().newBuilder()
                .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .writeTimeout(writeTimeout, TimeUnit.SECONDS)
                .build();
    }
}
