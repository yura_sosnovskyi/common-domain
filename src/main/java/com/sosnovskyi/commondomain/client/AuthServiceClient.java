package com.sosnovskyi.commondomain.client;

import com.sosnovskyi.commondomain.dto.UserOutputDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "authServiceClient", url = "${auth.service.base.url}")
public interface AuthServiceClient {

    @GetMapping("/auth/token/validate")
    ResponseEntity<UserOutputDto> validateToken(@RequestParam String token);

}
