package com.sosnovskyi.commondomain.exception;

import com.sosnovskyi.commondomain.exception.enums.ErrorType;

public class UserAlreadyExistsException extends ServiceException {

    private static final String DEFAULT_MESSAGE = "User with email %s already exists, cannot save him!";

    public UserAlreadyExistsException(String email) {
        super(String.format(DEFAULT_MESSAGE, email));
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.PROCESSING_ERROR_TYPE;
    }
}
