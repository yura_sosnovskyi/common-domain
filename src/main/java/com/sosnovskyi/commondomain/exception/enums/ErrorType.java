package com.sosnovskyi.commondomain.exception.enums;

public enum ErrorType {
    VALIDATION_ERROR_TYPE,
    AUTHORIZATION_ERROR_TYPE,
    PROCESSING_ERROR_TYPE,
    DATABASE_ERROR_TYPE,
    FATAL_ERROR_TYPE
}
