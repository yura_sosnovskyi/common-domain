package com.sosnovskyi.commondomain.mapper;

import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.commondomain.dto.UserInputDto;
import com.sosnovskyi.commondomain.dto.UserOutputDto;
import com.sosnovskyi.commondomain.model.Role;
import com.sosnovskyi.commondomain.model.User;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserMapper {
    public static User fromInputDto(UserInputDto dto, List<Role> roles) {
        return new User(dto.getEmail(),
                dto.getFirstname(),
                dto.getLastname(),
                dto.getPhoneNumber(),
                dto.getPassword(),
                dto.getCity(),
                dto.getAge(),
                dto.getGender(),
                roles);
    }

    public static UserOutputDto toOutputDto(User user) {
        Set<SpecialistOutputDto> lastVisited =
                user.getLastVisitedSpecialists().stream()
                        .map(SpecialistMapper::lastVisitedToOutputDto)
                        .collect(Collectors.toSet());

        return new UserOutputDto(user.getId(),
                user.getFirstname(),
                user.getLastname(),
                user.getEmail(),
                user.getPhoneNumber(),
                user.getCity(),
                user.getAge(),
                user.getGender(),
                user.getRoles().stream()
                        .map(Role::getName)
                        .collect(Collectors.toList()),
                lastVisited,
                user.getCreatedBy(),
                user.getUpdatedBy(),
                user.getCreatedAt(),
                user.getUpdatedAt());
    }
}
