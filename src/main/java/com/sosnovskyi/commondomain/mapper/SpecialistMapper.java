package com.sosnovskyi.commondomain.mapper;

import com.sosnovskyi.commondomain.dto.FeedbackOutputDto;
import com.sosnovskyi.commondomain.dto.SpecialistInputDto;
import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.commondomain.model.Category;
import com.sosnovskyi.commondomain.model.Role;
import com.sosnovskyi.commondomain.model.Specialist;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SpecialistMapper {
    public static Specialist fromInputDto(SpecialistInputDto dto,
                                          List<Role> roles,
                                          List<Category> categories) {
        return new Specialist(dto.getEmail(),
                dto.getFirstname(),
                dto.getLastname(),
                dto.getPhoneNumber(),
                dto.getPassword(),
                dto.getCity(),
                dto.getAge(),
                dto.getGender(),
                roles,
                dto.getTitle(),
                dto.getEducationType(),
                dto.getEducationInstitution(),
                dto.getDescription(),
                dto.getCardNumber(),
                dto.getExperience(),
                categories);
    }

    public static SpecialistOutputDto toOutputDto(Specialist specialist) {
        List<String> roles = specialist.getRoles().stream()
                .map(Role::getName)
                .collect(Collectors.toList());
        List<String> categories = specialist.getCategories().stream()
                .map(Category::getName)
                .collect(Collectors.toList());
        List<FeedbackOutputDto> feedbackOutputDtos = specialist.getFeedbacks()
                .stream()
                .map(FeedbackMapper::toOutputDto)
                .collect(Collectors.toList());
        Set<SpecialistOutputDto> lastVisited =
                specialist.getLastVisitedSpecialists().stream()
                        .map(SpecialistMapper::lastVisitedToOutputDto)
                        .collect(Collectors.toSet());
        return new SpecialistOutputDto(specialist.getId(),
                specialist.getEmail(),
                specialist.getFirstname(),
                specialist.getLastname(),
                specialist.getPhoneNumber(),
                specialist.getCity(),
                specialist.getAge(),
                specialist.getGender(),
                roles,
                specialist.getTitle(),
                specialist.getEducationType(),
                specialist.getEducationInstitution(),
                specialist.getDescription(),
                specialist.getRating(),
                specialist.getIsVerified(),
                feedbackOutputDtos,
                categories,
                specialist.getExperience(),
                lastVisited,
                specialist.getCreatedBy(),
                specialist.getUpdatedBy(),
                specialist.getCreatedAt(),
                specialist.getUpdatedAt());
    }

    public static SpecialistOutputDto lastVisitedToOutputDto(Specialist specialist) {
        List<String> categories = specialist.getCategories().stream()
                .map(Category::getName)
                .collect(Collectors.toList());
        List<String> roles = specialist.getRoles().stream()
                .map(Role::getName)
                .collect(Collectors.toList());
        return new SpecialistOutputDto(specialist.getId(),
                specialist.getEmail(),
                specialist.getFirstname(),
                specialist.getLastname(),
                specialist.getPhoneNumber(),
                specialist.getCity(),
                specialist.getAge(),
                specialist.getGender(),
                roles,
                specialist.getTitle(),
                specialist.getEducationType(),
                specialist.getEducationInstitution(),
                specialist.getDescription(),
                specialist.getRating(),
                specialist.getIsVerified(),
                null,
                categories,
                specialist.getExperience(),
                null,
                specialist.getCreatedBy(),
                specialist.getUpdatedBy(),
                specialist.getCreatedAt(),
                specialist.getUpdatedAt());
    }
}
