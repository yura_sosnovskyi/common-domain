package com.sosnovskyi.commondomain.mapper;

import com.sosnovskyi.commondomain.dto.FeedbackInputDto;
import com.sosnovskyi.commondomain.dto.FeedbackOutputDto;
import com.sosnovskyi.commondomain.model.Feedback;
import com.sosnovskyi.commondomain.model.Specialist;
import com.sosnovskyi.commondomain.model.User;

public class FeedbackMapper {
    public static Feedback fromInputDto(FeedbackInputDto dto, Specialist specialist, User reviewer) {
        return new Feedback(dto.getMessage(), dto.getRating(), specialist, reviewer);
    }

    public static FeedbackOutputDto toOutputDto(Feedback feedback) {
        return new FeedbackOutputDto(feedback.getId(),
                feedback.getMessage(),
                feedback.getRating(),
                feedback.getSpecialist().getId(),
                feedback.getReviewer().getId(),
                feedback.getReviewer().getFirstname() + " "
                        + feedback.getReviewer().getLastname(),
                feedback.getCreatedBy(),
                feedback.getUpdatedBy(),
                feedback.getCreatedAt(),
                feedback.getUpdatedAt());
    }
}
