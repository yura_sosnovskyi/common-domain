package com.sosnovskyi.commondomain.mapper;

import com.sosnovskyi.commondomain.dto.AdminInputDto;
import com.sosnovskyi.commondomain.dto.AdminOutputDto;
import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.commondomain.model.Admin;
import com.sosnovskyi.commondomain.model.Role;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AdminMapper {
    public static Admin fromInputDto(AdminInputDto dto, List<Role> roles) {
        return new Admin(dto.getEmail(),
                dto.getFirstname(),
                dto.getLastname(),
                dto.getPhoneNumber(),
                dto.getCity(),
                dto.getPassword(),
                dto.getAge(),
                dto.getGender(),
                roles,
                dto.getTelegramUsername());
    }

    public static AdminOutputDto toOutputDto(Admin admin) {
        Set<SpecialistOutputDto> lastVisited =
                admin.getLastVisitedSpecialists().stream()
                        .map(SpecialistMapper::lastVisitedToOutputDto)
                        .collect(Collectors.toSet());
        return new AdminOutputDto(admin.getId(),
                admin.getEmail(),
                admin.getFirstname(),
                admin.getLastname(),
                admin.getPhoneNumber(),
                admin.getTelegramUsername(),
                admin.getCity(),
                admin.getAge(),
                admin.getGender(),
                admin.getRoles().stream()
                        .map(Role::getName)
                        .collect(Collectors.toList()),
                lastVisited,
                admin.getCreatedBy(),
                admin.getUpdatedBy(),
                admin.getCreatedAt(),
                admin.getUpdatedAt());
    }
}
