package com.sosnovskyi.commondomain.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@DiscriminatorValue("specialist")
public class Specialist extends User {

    @Column(name = "title")
    private String title;

    @Column(name = "edc_type")
    @Enumerated(EnumType.STRING)
    private EducationType educationType;

    @Column(name = "edc_institution")
    private String educationInstitution;

    @Column(name = "description")
    private String description;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "rating")
    private BigDecimal rating;

    @Column(name = "is_verified")
    private Boolean isVerified;

    @OneToMany(mappedBy = "specialist", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval=true)
    private List<Feedback> feedbacks;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "specialist_categories",
            joinColumns = @JoinColumn(name = "specialist_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> categories;

    @Column(name = "experience")
    private Integer experience;

    public Specialist(String email, String firstname,
                      String lastname, String phoneNumber,
                      String encodedPassword, String city,
                      Integer age, Gender gender, List<Role> roles,
                      String title, EducationType educationType,
                      String educationInstitution, String description,
                      String cardNumber, Integer experience, List<Category> categories) {
        super(email, firstname, lastname, phoneNumber, encodedPassword, city, age, gender, roles);
        this.title = title;
        this.educationType = educationType;
        this.educationInstitution = educationInstitution;
        this.description = description;
        this.cardNumber = cardNumber;
        this.experience = experience;
        this.categories = categories;
        this.rating = new BigDecimal("0.0");
        this.isVerified = false;
        this.feedbacks = new ArrayList<>();
    }
}
