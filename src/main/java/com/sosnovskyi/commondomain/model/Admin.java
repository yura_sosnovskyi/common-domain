package com.sosnovskyi.commondomain.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@DiscriminatorValue("admin")
public class Admin extends User {

    @Column(name = "telegram_username")
    private String telegramUsername;

    public Admin(String email, String firstname,
                 String lastname, String phoneNumber,
                 String city, String encodedPassword,
                 Integer age, Gender gender, List<Role> roles,
                 String telegramUsername) {
        super(email, firstname, lastname, phoneNumber,
                encodedPassword, city, age, gender, roles);
        this.telegramUsername = telegramUsername;
    }
}
