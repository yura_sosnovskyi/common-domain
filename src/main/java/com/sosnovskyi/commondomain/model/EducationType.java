package com.sosnovskyi.commondomain.model;

public enum EducationType {
    PRIMARY,
    SECONDARY,
    BACHELOR,
    MASTER,
    DOCTORAL
}
