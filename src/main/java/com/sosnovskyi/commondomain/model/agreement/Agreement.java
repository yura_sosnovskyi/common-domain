package com.sosnovskyi.commondomain.model.agreement;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "agreement")
public class Agreement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "specialist_id", updatable = false)
    private Long specialistId;

    @Column(name = "user_id", updatable = false)
    private String userId;

    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name = "criteria", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "`criteria_description`")
    List<String> acCriteria;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "amount", column = @Column(name = "price_amount", precision = 15, scale = 6)),
            @AttributeOverride(name = "currency", column = @Column(name = "currency", precision = 15, scale = 6))
    })
    private Price price;

    @Enumerated(EnumType.STRING)
    private AgreementState state;

    @OneToOne(mappedBy = "agreement")
    @JsonBackReference
    @ToString.Exclude
    protected Payment payment;

    @CreatedBy
    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @LastModifiedBy
    @Column(name = "updated_by")
    private String updatedBy;

    @CreatedDate
    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;
}
