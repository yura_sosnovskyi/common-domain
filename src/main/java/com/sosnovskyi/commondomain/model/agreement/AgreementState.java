package com.sosnovskyi.commondomain.model.agreement;

public enum AgreementState {
    PENDING,
    PENDING_APPROVAL,
    PENDING_PAYMENT,
    ON_DISPUTE,
    SUCCESSFUL,
    TERMINATED
}
