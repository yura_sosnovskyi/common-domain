package com.sosnovskyi.commondomain.model.agreement;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "payment")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_merchant_id", updatable = false)
    private Long firstMerchantId;

    @Column(name = "second_merchant_id", updatable = false)
    private Long secondMerchantId;

    @Column(name = "payer_id", updatable = false)
    private String payerId;

    private String description;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "amount", column = @Column(name = "first_price_amount", precision = 15, scale = 6)),
            @AttributeOverride(name = "currency", column = @Column(name = "first_currency", precision = 15, scale = 6))
    })
    private Price firstAmount;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "amount", column = @Column(name = "second_price_amount", precision = 15, scale = 6)),
            @AttributeOverride(name = "currency", column = @Column(name = "second_currency", precision = 15, scale = 6))
    })
    private Price secondAmount;

    @Enumerated(EnumType.STRING)
    private PaymentStatus state;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "agreement_id", referencedColumnName = "id")
    @JsonManagedReference
    private Agreement agreement;

    @CreatedBy
    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @LastModifiedBy
    @Column(name = "updated_by")
    private String updatedBy;

    @CreatedDate
    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;
}
