package com.sosnovskyi.commondomain.model.agreement;

public enum PaymentStatus {
    APPROVED,
    DECLINED
}
