package com.sosnovskyi.commondomain.model;

public enum Gender {
    MALE, FEMALE, NON_SPECIFIED
}
